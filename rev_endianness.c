#include<stdio.h>
#include<stdint.h>

/* Change the integer pointed to by "value"
 * with its endianness inverted.
 *
 */
void reverse_in_place( uint32_t *value){
	uint8_t *pvalue = (uint8_t *)value;
	uint8_t tmp = *pvalue;
	*pvalue = pvalue[ 3];	
	pvalue[ 3] = tmp;
	tmp = pvalue[ 1];
	pvalue[ 1] = pvalue[ 2];
	pvalue[ 2] = tmp;
}

/* Return "value" in reverse endianness.
 *
 */
uint32_t reverse( uint32_t value){
	uint32_t out;
	uint8_t *pout = (uint8_t *)&out, *pvalue = (uint8_t *)&value;
	for( int i = 0; i < 4; i++){
		pout[ i] = pvalue[ 3-i];
	}
	return out;
}

/* Test reverse and reverse_in_place functions.
 *
 */
int main( void){
	uint32_t value = 0x12345678UL;
	uint32_t rev = reverse( value);
	printf("value = 0x%.8x; reversed = 0x%.8x\n", value, rev);
	uint32_t val = 0x12345678UL;
	printf("value = 0x%.8x", val);
	reverse_in_place( &val);
	printf("; reversed = 0x%.8x\n", val);
}

